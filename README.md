# Консольная программа поисковик ссылок

## Принцип работы

Программа находит в интернете начиная от стартовой точки все ссылки на вэб-странице в заданном количестве (название и саму ссылку).
Если поиск не рекурсивный, то ссылки берутся только из поисковика. В противном случае берется первая "поисковая" ссылка, а дальше переходим по ней и ищем ссылки вглубь.

## Подробности реализации

Пользователь задает:
* текст запроса;
* поисковую систему (google.com, yandex.ru, ...);
* количество результатов;
* рекурсивный ли поиск;
* формат результата (вывод в консоль, csv, json).