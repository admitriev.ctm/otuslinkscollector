from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="otuslinkscollector",
    version="0.0.1",
    author="Andrei Dmitriev",
    author_email="a.dmitriev.ctm@gmail.com",
    description="Сборщик ссылок",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=[
        'otuslinkscollector',
        'otuslinkscollector\search_engine'
    ],
    entry_points={
        'console_scripts': ['collectlinks=otuslinkscollector:run']
    }
)

