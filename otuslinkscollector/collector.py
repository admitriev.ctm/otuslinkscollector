import csv
import json
from optparse import OptionParser
from otuslinkscollector.search_engine import Google, SearchEngineError


def parse_options():
    """Разбирает аргументы командной строки"""
    option_parser = OptionParser(
        usage="collector [options]")
    option_parser.add_option(
        '-t', '--text',
        dest='query_text',
        default='курсы otus',
        help='Текст поискового запроса'
    )
    option_parser.add_option(
        '-e', '--engine',
        dest='search_engine',
        default='google.com',
        help='Устанавливает поисковую систему (google.com, yandex.ru)'
    )
    option_parser.add_option(
        '-l', '--limit',
        dest='links_limit',
        type="int",
        default=10,
        help='Задает ограничение количества собираемых ссылок'
    )
    option_parser.add_option(
        '-r', '--recursive',
        action='store_true',
        dest='recursive',
        default=False,
        help='Включает рекурсивный поиск ссылок (находим первую ссылку,'
             ' переходим по ней, находим все ссылки на странице,'
             ' переходим по ним, ...)'
    )
    option_parser.add_option(
        '-o', '--output-format',
        dest='output_format',
        default='CSV',
        help='Задает формат сохранения результаов - JSON или CSV. '
             'Если формат не выбран - результаты выводятся на консоль'
    )

    return option_parser.parse_args()


def collect_links(options) -> list:
    """Возвращает список найденных ссылок по заданым параметрам (options)"""
    engine = None

    if 'google' in options.search_engine:
        engine = Google(options.query_text)
    elif 'yandex' in options.search_engine:
        ...
        # engine = Yandex(options.query_text)
    else:
        raise SearchEngineError(options.search_engine)

    result_links = engine.search_links(
        results_limit=options.links_limit,
        recursive_search=options.recursive
    )

    return result_links


def export_data(data: list, output_format: str = 'console') -> None:
    """Экспортирует данные (data) в соответствующем формате (output_format)

    :param data: данные для экспорта
    :param output_format: формат (CSV, JSON, Console)"""

    output_format = output_format.lower()

    if output_format == 'json':
        with open('result_links.json', 'w', encoding='utf8') as json_file:
            dict_data = [{'link': x[0], 'description': x[1]} for x in data]
            json.dump(dict_data, json_file, ensure_ascii=False)
    elif output_format == 'csv':
        with open('result_links.csv',
                  'w',
                  newline='',
                  encoding='utf8') as csv_file:
            writer = csv.writer(csv_file,
                                delimiter=',',
                                quoting=csv.QUOTE_MINIMAL)
            writer.writerow(['link', 'description'])
            writer.writerows(data)
    else:
        for row in data:
            print(row)


def run():
    # разбираем аргументы командной строки
    (opts, args) = parse_options()

    # получаем
    links = collect_links(options=opts)

    # экспортируем результат поиска
    export_data(data=links, output_format=opts.output_format)


if __name__ == '__main__':
    run()
