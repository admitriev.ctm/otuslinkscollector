from bs4 import BeautifulSoup
from typing import Union
import random
import requests
import time

HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
                  " AppleWebKit/537.36 (KHTML, like Gecko)"
                  " Chrome/70.0.3538.110 Safari/537.36",
    "Accept-Encoding": "gzip, deflate",
    "Accept": "text/html,application/xhtml+xml,application/xml;"
              "q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Language": "ru"
}


def _extract_links_info(page_content: str) -> list:
    """
    Извлекает информацию о ссылках на результаты поиска
    из содержимого HTML-страницы (page_content)

    Возвращает список пар значений [ссылка, описание]
    """
    links_info = []
    soup = BeautifulSoup(page_content, "html.parser")
    search_block = soup.find("div", id="search")
    div_tags = search_block.find_all("div", class_="rc")
    for div_tag in div_tags:
        a_tags = div_tag.find_all('a', class_=None)
        for a_tag in a_tags:
            if a_tag.find('h3'):
                info = [a_tag.get('href'), a_tag.find('h3').text]
                links_info.append(info)
    return links_info


def _extract_next_url(page_content: str) -> Union[str, None]:
    """
    Извлекает ссылку на следующую страницу с результатами поиска.

    Если ссылка не найдена, возвращает None
    """
    next_url = None

    soup = BeautifulSoup(page_content, "html.parser")
    a_next = soup.find("a", id="pnnext")
    if a_next:
        next_url = a_next.get("href")
        next_url = f"http://google.com{next_url}"

    return next_url


def _extract_all_links(page_content: str) -> Union[list, None]:
    """
    Извлекает все ссылки на HTML-странице (page_content)

    Если ссылки не найдены, возвращает None
    """
    links = []

    soup = BeautifulSoup(page_content, "html.parser")
    a_tags = soup.find_all("a", href=True)

    if not a_tags:
        return None

    for a_tag in a_tags:
        if a_tag.get('href').startswith('https://') and a_tag.text:
            link_info = [
                a_tag.get('href'),
                str.strip(a_tag.text)
            ]
            links.append(link_info)

    return links


class Google:
    """ Работа с поисковой системой Google """

    def __init__(self, query: str):
        """
        Инициализация "поисковой системы".
        Создаётся сессия и начальная ссылка для получения первых результатов
        """
        self.session = requests.Session()
        self.session.headers = HEADERS
        query = query.replace(' ', '+')
        self.url = f"http://google.com/search?q={query}"

    def search_links(self,
                     results_limit: int,
                     recursive_search: bool = False) -> list:
        """
        Находит указанное в results_limit количество пар [ссылка, описание]
        """
        if results_limit <= 0:
            raise ValueError('Максимальное количество результатов'
                             ' (result_limit) должно быть больше нуля')

        if recursive_search:
            results_data = self._get_data(1)
            url = results_data[0][0]
            results_data = self._get_data_recursive(
                url=url,
                data=results_data,
                results_limit=results_limit)
        else:
            results_data = self._get_data(results_limit)

        return results_data

    def _get_data_recursive(self,
                            url: str,
                            data: list,
                            results_limit: int) -> list:
        """
        Рекурсивно получает информацию о ссылках.
        Находит первую ссылку по запросу, а далее ищет ссылки вглубь.

        Возвращает список с указанным в results_limit
        количеством пар [ссылка, описание]
        """

        if len(data) >= results_limit:
            return data[:results_limit]

        page_content = self._get_page_content(url)
        all_links = _extract_all_links(page_content)
        if not all_links:
            return data[:results_limit]

        url = all_links[0][0]
        data.extend(all_links)
        return self._get_data_recursive(
            url=url,
            data=data,
            results_limit=results_limit)

    def _get_data(self, results_limit: int) -> list:
        """
        Получает информацию о ссылках из результатов поиска.

        Возвращает список с указанным в results_limit
        количеством пар [ссылка, описание]
        """
        data = []
        url = self.url
        while len(data) < results_limit:
            page_content = self._get_page_content(url)
            links_info = _extract_links_info(page_content)
            for link_info in links_info:
                data.append(link_info)
            url = _extract_next_url(page_content)
            if not url:
                break
        return data[:results_limit]

    def _get_page_content(self, url: str) -> str:
        """
        Получает html-страницу по ссылке (url).

        Возвращает контент страницы или пустую строку,
        если страница не получено успешно
        """
        response = self.session.get(url)
        time.sleep(random.randrange(3, 11, 1))
        if response.status_code == 200:
            return response.text
        return ""

    def __del__(self):
        """ Закрывает сессию при удалении объекта """
        self.session.close()
