class SearchEngineError(Exception):
    """Возникает если не удалось определить поисковую систему

    Attributes:
        engine -- поисковая система
        message -- описание ошибки"""

    def __init__(self, engine, message='Поисковая система не из списка:'
                                       ' google.com, yandex.ru'):
        self.engine = engine
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'"{self.engine}" -> {self.message}'
